using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Facebook;

namespace Android_Hello_World
{
	class FacebookUtils
	{
		private static string APP_ID = "315324461947229";

		private static string APP_EXTENDED_PERMISSIONS = "user_birthday, read_friendlists";

		private static object REDIRECT_FACEBOOK_URL = "https://www.facebook.com/connect/login_success.html";

		private static object MESSAGE_INIT = "Facebook Login ";

		private static string MESSAGE_FAILED_LOGIN = "Failed to Log In Facebook";

		public const string MESSAGE_LOADING = "Loading Facebook";

		private static string MESSAGE_NOT_LOGGED_IN = "Not Logged In Facebook";

		private static string MESSAGE_INFO_NOT_LOADED = "Couldn't Load Info from Facebook";

		private static string FACEBOOK_SCREEN_LABEL = "Facebook Login";

		private static string RESPONSE_TYPE = "token";

		private static string DISPLAY_TYPE = "touch";

		const string FacebookAppId = "315324461947229";

		public static void PostToMyWall (String userToken)
		{

			FacebookClient fb = new FacebookClient (userToken);
			String myMessage = "Hello from Xamarin";

			fb.PostTaskAsync ("me/feed", new { message = myMessage }).ContinueWith (t => {
				if (!t.IsFaulted) {
					String message = "Great, your message has been posted to you wall!";
					Console.WriteLine (message);
				}
			});
		}

		public static void GetMyInfo (String extendedPermissions)
		{
			String url = GetFacebookLoginUrl (FacebookAppId, extendedPermissions);
			String userToken = getToken (url);
			// This uses Facebook Graph API
			// See https://developers.facebook.com/docs/reference/api/ for more information.
			FacebookClient fb = new FacebookClient (userToken);

			fb.GetTaskAsync ("me").ContinueWith (t => {
				if (!t.IsFaulted) {
					var result = (IDictionary<String, object>)t.Result;
					String myDetails = String.Format ("Your name is: {0} {1} and your Facebook profile Url is: {3}", 
						(String)result["first_name"], (String)result["last_name"], 
						(String)result["link"]);
					Console.WriteLine (myDetails);
				}
			});
		}

		public static void PrintFriendsNames (String userToken)
		{
			var query = String.Format("SELECT uid,name,pic_square FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1={0}) ORDER BY name ASC", "me()");
			FacebookClient fb = new FacebookClient (userToken);

			fb.GetTaskAsync ("fql", new { q = query }).ContinueWith (t => {
				if (!t.IsFaulted) {
					var result = (IDictionary<String, object>)t.Result;
					var data = (IList<object>)result["data"];
					var count = data.Count;
					var message = String.Format ("You have {0} friends", count);
					Console.WriteLine (message);

					foreach (IDictionary<String, object> friend in data)
						Console.WriteLine ((String) friend["name"]);
				}
			});
		}

		public static string getToken(String url)
		{

			var fb = new FacebookClient ();
			FacebookOAuthResult oauthResult;
			if (!fb.TryParseOAuthCallbackUrl (new Uri (url), out oauthResult))
			{
				return "";
			}

			if (oauthResult.IsSuccess)
			{
				var accessToken = oauthResult.AccessToken;
				return accessToken;
			}
			else
			{
				return "";
			}
		}

		public static string GetFacebookLoginUrl (String appId, string extendedPermissions)
		{
			var parameters = new Dictionary<String, object>();
			parameters["client_id"] = appId;
			parameters["redirect_uri"] = getRedirectFacebookUri();
			parameters["response_type"] = "token";
			parameters["display"] = "touch";

			if (!String.IsNullOrEmpty (extendedPermissions))
			{
				parameters["scope"] = extendedPermissions;
			}

			var fb = new FacebookClient ();
			return fb.GetLoginUrl(parameters).AbsoluteUri;
		}

		public static string getAppId(){
			return APP_ID;
		}

		public static string getExtendedPermissions(){
			return APP_EXTENDED_PERMISSIONS;
		}

		public static object getInitMEssage ()
		{
			return MESSAGE_INIT;
		}

		public static string getprofilePictureUrl(String userId, string accessToken){
			return String.Format("https://graph.facebook.com/{0}/picture?type={1}&access_token={2}", userId, "square", accessToken);
		}

		public static object getRedirectFacebookUri ()
		{
			return REDIRECT_FACEBOOK_URL;
		}

		public static string getFailedLoginMessage ()
		{
			return MESSAGE_FAILED_LOGIN;
		}

		public static string getFacebookScreenLabel(){
			return FACEBOOK_SCREEN_LABEL;
		}

		public static string getLoadingMessage ()
		{
			return MESSAGE_LOADING;
		}

		public static string getResponseType ()
		{
			return RESPONSE_TYPE;
		}

		public static string getDisplayType ()
		{
			return DISPLAY_TYPE;
		}

		public static string getNotLoggedInFacebook ()
		{
			return MESSAGE_NOT_LOGGED_IN;
		}

		public static string getErrorLoadingInfo ()
		{
			return MESSAGE_INFO_NOT_LOADED;
		}
	}

}

