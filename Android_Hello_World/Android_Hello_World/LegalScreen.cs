using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Json;

namespace Android_Hello_World
{
	[Activity (Label = "LegalScreen")]			
	public class LegalScreen : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening LegalScreen...");
				base.OnCreate (bundle);
				SetContentView (Resource.Layout.legal);
				var buttonLegalOk = FindViewById<Button> (Resource.Id.okButton);
				TextView legalText = FindViewById<TextView> (Resource.Id.legalTextView);
//				legalText.Text = Session.getSession ().getParameter ("bases");
				
				buttonLegalOk.Click += (sender, e) => {
					Finish ();
			};
			AndroidUtils.trackView(this, "LegalScreen");
		}

		protected override void OnResume(){
			base.OnResume ();
		}

	}
}