using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Android_Hello_World
{
	[Activity (Label = "GoodResultScreen")]			
	public class GoodResultScreen : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening GoodResultScreen...");
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.result_good);
			var aImage = FindViewById<ImageView> (Resource.Id.prizeprize);
			aImage.Click += (sender, e) => {
				StartActivity(typeof(FacebookScreen));

			};  
		}
	}
}

