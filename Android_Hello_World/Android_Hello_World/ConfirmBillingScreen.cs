using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;

namespace Android_Hello_World
{
	[Activity (Label = "ConfirmBillingScreen")]			
	public class ConfirmBillingScreen : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening ConfirmBillingScreen...");
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.confirm_billing);
		}
	}
}

