using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Facebook;
using Android.Webkit;

namespace Android_Hello_World
{
	[Activity (Label = FacebookUtils.MESSAGE_LOADING)]			
	public class FacebookScreen : Activity
	{
		FacebookClient fb;
		string url;
		string appId;
		string extendedPermissions;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			fb = new FacebookClient ();
			appId = Intent.GetStringExtra ("AppId");
			extendedPermissions = Intent.GetStringExtra ("ExtendedPermissions");
			url = GetFacebookLoginUrl (appId, extendedPermissions);

			WebView webView = new WebView(this);
			webView.Settings.JavaScriptEnabled = true;
			webView.Settings.SetSupportZoom(true);
			webView.Settings.BuiltInZoomControls = true;
			webView.Settings.LoadWithOverviewMode = true;
			webView.ScrollBarStyle = ScrollbarStyles.OutsideOverlay;
			webView.ScrollbarFadingEnabled = true;


			webView.VerticalScrollBarEnabled = true;
			webView.HorizontalScrollBarEnabled = true;

			webView.SetWebViewClient(new FBWebClient (this));
			webView.SetWebChromeClient(new FBWebChromeClient (this));

			AddContentView(webView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FillParent, ViewGroup.LayoutParams.FillParent));

			webView.LoadUrl(url);
		}

		private string GetFacebookLoginUrl (string appId, string extendedPermissions)
		{
			var parameters = new Dictionary<string, object>();
			parameters["client_id"] = appId;
			parameters ["redirect_uri"] = FacebookUtils.getRedirectFacebookUri ();
			parameters["response_type"] = FacebookUtils.getResponseType();
			parameters["display"] = FacebookUtils.getDisplayType();

			// add the 'scope' only if we have extendedPermissions.
			if (!string.IsNullOrEmpty (extendedPermissions))
			{
				// A comma-delimited list of permissions
				parameters["scope"] = extendedPermissions;
			}

			return fb.GetLoginUrl(parameters).AbsoluteUri;
		}



		private class FBWebChromeClient : WebChromeClient
		{
			private Activity mParentActivity;

			public FBWebChromeClient (Activity parentActivity)
			{
				mParentActivity = parentActivity;
			}

			public override void OnProgressChanged(WebView view, int newProgress)
			{
				mParentActivity.Title = string.Format(FacebookUtils.getLoadingMessage() + " {0}%", newProgress);
				mParentActivity.SetProgress(newProgress * 100);

				if (newProgress == 100) {
					mParentActivity.Title = view.Url;
				}
			}
		}
	}
}

