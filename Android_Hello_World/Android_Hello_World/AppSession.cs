using System;
using System.Json;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.IO;


namespace Android_Hello_World
{
	public class AppSession
	{
		private static AppSession session;

		private static bool started;
		private string bases;
		private string signature;
		private Dictionary<string, string> parameters;
		private bool onLine;
		JsonValue startDataJson;

		private static string SETTINGS_FILENAME = "Luli.Settings";
		private static string KEY_BASES_PARAMETER = "bases";
		private static string OFFLINE_SUFFIX = "\n [BE CAREFUL. OFFLINE MODE ON. POSSIBLE NOT UPDATED APPLICATION.]";

		private static string USER_PIN = "666.666";
		string USER_SIM_ID = "01";
		string USER_APP_NAME = "proto1";
		private static string USER_TAG="nada";
		private static string USER_APP_VERSION="0.7";

		private string BASE_PATH = Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData);

		private AppSession(){

			AppSession.started = true;
			this.parameters = new Dictionary<string, string> ();
			if (ServiceRequest.servicesAvailables()==true) {
				this.onLine = true;
				this.goOnline ();
			} else if(Utils.existFile(SETTINGS_FILENAME)){
				this.onLine = false;
				this.goOffline ();
			}

		}


		public static AppSession createSession ()
		{
			Console.WriteLine ("Creating Session... ");
			if (AppSession.session == null) {
				AppSession.session = new AppSession ();

			}
			return AppSession.session;
		}

		public static AppSession getSession(){
			Console.WriteLine ("Returning Session... ");
			if(AppSession.session==null){
				return AppSession.createSession ();
			}
			return AppSession.session;
		}

		public string getBases(){
			return (this.onLine?this.parameters[KEY_BASES_PARAMETER]:this.parameters[KEY_BASES_PARAMETER]+OFFLINE_SUFFIX);
		}

		public void setParameter(string parameter, string value){
			this.parameters.Add (parameter, value);
		}

		public string getParameter(string parameter){
			if(this.parameters.ContainsKey(parameter)){
				return (this.onLine?this.parameters[parameter]:this.parameters[parameter]+OFFLINE_SUFFIX);
			}
			return "";
		}

		private void loadFromStorage ()
		{
			Console.WriteLine ("Loading from storage...");
			string filename = Path.Combine(BASE_PATH, SETTINGS_FILENAME);
			Dictionary<string, string> parametersDictionary = Utils.readParametersFromFile (filename);
			if(parametersDictionary==null){
				this.noParametersFound ();
				return;
			}
			this.parameters = parametersDictionary;
			if (this.parameters.ContainsKey (KEY_BASES_PARAMETER)) {
				this.bases = this.parameters [KEY_BASES_PARAMETER];
			} else {
				this.bases = "Something is missing. Parameter BASES not present in settings file.";
			}
		}

		public void goOffline(){
			Console.WriteLine ("Going OffLine...");
			this.onLine = false;
			this.loadFromStorage ();
		}

		public void goOnline(){
			Console.WriteLine ("Going OnLine...");
			this.onLine = true;
			this.loadFromServices ();
		}



		private void loadFromServices ()
		{
			Console.WriteLine ("Loading from Movix-Services... ");
			Console.WriteLine ("Using pin= "+ USER_PIN +"simId="+ USER_SIM_ID+"appName="+USER_APP_NAME);
			this.startDataJson = ServiceRequest.startDataRequest (USER_PIN, USER_SIM_ID, USER_APP_NAME);
			Console.WriteLine ("Using tag="+USER_TAG+"appName="+USER_APP_NAME+"appVersion="+USER_APP_VERSION+"simId"+USER_SIM_ID);
			JsonValue preData = ServiceRequest.preStartDataRequest (USER_TAG, USER_APP_NAME, USER_APP_VERSION, USER_SIM_ID);
			if(this.startDataJson==null){
				Console.WriteLine ("Error trying to get startDataJson.");
				this.goOffline ();
				return;
			}
			this.setParameter (KEY_BASES_PARAMETER, this.startDataJson[KEY_BASES_PARAMETER]);
			this.saveToParametersFile ();
		}

		private void saveToParametersFile ()
		{
			Console.WriteLine ("Saving parameters from Session...");
			string dataText = "";
			if(this.startDataJson.ContainsKey(KEY_BASES_PARAMETER)){
				dataText += "bases="+this.startDataJson[KEY_BASES_PARAMETER];
			}
			Utils.saveToStorage (BASE_PATH,SETTINGS_FILENAME,dataText);

		}

		void noParametersFound ()
		{
			Console.WriteLine ("Missing parameters file or wrong path.");
			this.parameters = new Dictionary<string, string>();
			this.bases = "Something is missing. Parameter BASES not present in settings file.";
			this.parameters.Add ("bases", this.bases);

		}


	}
}

