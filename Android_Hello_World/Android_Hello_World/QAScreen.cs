using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;

namespace Android_Hello_World
{
	[Activity (Label = "QAScreen")]			
	public class QAScreen : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening QAScreen...");
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.qa);

			var layout = FindViewById<LinearLayout> (Resource.Layout.home);


			var aImage = FindViewById<ImageView> (Resource.Id.qaImagePrice);
			aImage.Click += (sender, e) => {
				StartActivity(typeof(PrizeScreen));
			};  
		}
	}
}