using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Social.Services;
using Xamarin.Social;

namespace Android_Hello_World
{
	[Activity (Label = "PrizeScreen")]			
	public class PrizeScreen : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening PriceScreen...");
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.prize);
			var buttonPriceOk = FindViewById<Button>(Resource.Id.okButton);

			buttonPriceOk.Click += (sender, e) => {
				this.Finish();
			}; 
			ImageView share = FindViewById<ImageView> (Resource.Id.prizeprize);
			share.Click += (sender, e) => {
				var facebook = new FacebookService { ClientId = FacebookUtils.getAppId () };

				var item = new Item { Text = "I love Luli" };
				item.Links.Add (new Uri ("http://movix.com"));
				var shareIntent = facebook.GetShareUI (this, item, result => {
				});
				StartActivityForResult (shareIntent, 42);
			};

		}
	}
}

