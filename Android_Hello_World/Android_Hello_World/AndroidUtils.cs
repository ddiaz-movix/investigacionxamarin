using System;

using Android.Content;
using Android.Telephony;
using Android.App;
using Android.Runtime;
using Android.Util;
using Google.Analytics.Tracking;
using System.Collections.Generic;

namespace Android_Hello_World
{
	public class AndroidUtils
	{
		public AndroidUtils ()
		{
		}

		public static  void sendSms(string to, string message){
			Console.WriteLine ("Sending SMS to= "+to+" message= "+message);
			SmsManager.Default.SendTextMessage (to, null,
				message, null, null);
		}

		public static void receiveSms(Android.Content.Context context){
			Console.WriteLine ("Regitering receiveSms...");
			IntentFilter filter = new IntentFilter();
			filter.AddAction("android.provider.Telephony.SMS_RECEIVED");
			filter.Priority = 999;
			SmsBroadcast smsReceiver = new SmsBroadcast ();
			context.RegisterReceiver(smsReceiver, filter);
		}

		public static void Alert (string title, string message, bool CancelButton , Action<Result> callback, Context context)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.SetTitle(title);
			builder.SetIcon(Android.Resource.Id.Icon);
			builder.SetMessage(message);

			builder.SetPositiveButton("Ok", (sender, e) => {
				callback(Result.Ok);
			});

			if (CancelButton) {
				builder.SetNegativeButton("Cancel", (sender, e) => {
					callback(Result.Canceled);
				});
			}

			builder.Show();
		}

		public static void trackView(Context context, string viewInformation){
			if(Utils.needTrack()){
				Tracker tracker =GoogleAnalytics.GetInstance (context).GetTracker (Utils.getAnalyticsID ());
				GAServiceManager.Instance.SetLocalDispatchPeriod(5);
				Dictionary<string, string> information = new Dictionary<string, string>();
				information.Add(Fields.HitType, "appview");
				information.Add(Fields.ScreenName, viewInformation);
				tracker.Send (information);
			}
		}

	}
}

