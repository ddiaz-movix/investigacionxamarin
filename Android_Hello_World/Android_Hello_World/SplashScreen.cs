using System;
using System.Json;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;


namespace Android_Hello_World
{
	[Activity (Theme = "@drawable/bg_splash", Label = "Luli Cross Platform", MainLauncher = true)]
	public class SplashScreen : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{

			base.OnCreate (bundle);
			Console.WriteLine ("Starting app... ");
			SetContentView(Resource.Layout.splash);
			var aImage = FindViewById<ImageView> (Resource.Id.splashImage);
			aImage.Click += (sender, e) => {
				StartActivity(typeof(HomeScreen));
			};
		}
	}
}