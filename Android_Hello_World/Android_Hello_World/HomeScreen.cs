using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Facebook;
using Android.Graphics;
using Google.Analytics.Tracking;
using Xamarin.Social.Services;
using Xamarin.Social;

namespace Android_Hello_World
{
	[Activity (Label = "HomeScreen")]			
	public class HomeScreen : Activity
	{
		bool isLoggedIn;

		//Session session;

		public static Tracker tracker;

		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening HomeScreen...");
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.home);

			Button buttonPlay = FindViewById<Button> (Resource.Id.homeButtonPlay);
			Button buttonLegal = FindViewById<Button> (Resource.Id.mainButtonLegal);
			Button buttonHowTo = FindViewById<Button> (Resource.Id.mainButtonHowTo);
			Button buttonComment = FindViewById<Button> (Resource.Id.mainButtonComment);
			Button buttonId = FindViewById<Button> (Resource.Id.button);

			buttonId.Clickable = false;


			ImageView imagePrize = FindViewById<ImageView> (Resource.Id.homePrice);

			//this.session = Session.createSession ();
			buttonPlay.Click += (sender, e) => {
				StartActivity(typeof(BillingActivity));
			};

			buttonLegal.Click += (sender, e) => {
				StartActivity(typeof(LegalScreen));
			};  

			buttonHowTo.Click += (sender, e) => {
				StartActivity(typeof(HowToScreen));
			};  

			buttonComment.Click += (sender, e) => {
				StartActivity(typeof(CommentScreen));
			};

			imagePrize.Click += (sender, e) => {
				var webAuth = new Intent (this, typeof (FacebookScreen));
				webAuth.PutExtra ("AppId", FacebookUtils.getAppId());
				webAuth.PutExtra ("ExtendedPermissions", FacebookUtils.getExtendedPermissions());
				StartActivityForResult (webAuth, 0);
			};

			buttonId.Click += (object sender, EventArgs e) => {

			};


			//this.session = Utils.deserializeSession (Utils.serializeSession (this.session));
			//Console.WriteLine("Bases son: "+this.session.getBases ());
			Utils.testSql ();

		}

		protected override void OnResume(){
			base.OnResume ();
			Console.WriteLine ("Resuming HomeScreen..");

		}

		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);

			switch (resultCode) {
			case Result.Ok:

				string accessToken = data.GetStringExtra ("AccessToken");
				string userId = data.GetStringExtra ("UserId");
				string error = data.GetStringExtra ("Exception");

				FacebookClient fb = new FacebookClient (accessToken);

				ImageView imgUser = FindViewById<ImageView> (Resource.Id.imgUser);
				TextView txtvUserName = FindViewById<TextView> (Resource.Id.txtvUserName);

				fb.GetTaskAsync ("me").ContinueWith( t => {
					if (!t.IsFaulted) {

						var result = (IDictionary<string, object>)t.Result;

						string profilePictureUrl = FacebookUtils.getprofilePictureUrl(userId, accessToken);
						var bm = BitmapFactory.DecodeStream (new Java.Net.URL(profilePictureUrl).OpenStream());
						string profileName = (string)result["name"];

						RunOnUiThread (()=> {
							imgUser.SetImageBitmap (bm);
							txtvUserName.Text = profileName;
						});

						isLoggedIn = true;
					} else {
						AndroidUtils.Alert (FacebookUtils.getFailedLoginMessage(), "Reason: " + error, false, (res) => {} , this);
					}
				});

				break;
			case Result.Canceled:
				AndroidUtils.Alert (FacebookUtils.getFailedLoginMessage(), "User Cancelled", false, (res) => {}, this);
				break;
			default:
				break;
			}
		}

		protected override void OnStop(){
			base.OnStop ();
			if(Utils.needTrack()){
//				EasyTracker.GetInstance (this).ActivityStop (this);

			}
		}
	}
}