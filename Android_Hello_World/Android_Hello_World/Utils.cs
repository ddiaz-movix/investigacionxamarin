using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.Runtime.Remoting.Contexts;
using System.IO;
using System.Linq;
using Npgsql;
using Newtonsoft.Json;
using Xamarin.Social.Services;
using Xamarin.Social;
using System.ComponentModel;
using System.Threading.Tasks;
using Shared_MP.app.mp;

namespace Android_Hello_World
{
	public class Utils
	{
		//Category
		public static string ANALYTICS_MOUSE_CATEGORY = "Mouse Event";
		//Event Action
		public static string ANALYTICS_MOUSE_CLICK = "Mouse Click";
		//Value
		public static string ANALYTICS_MOUSE_PRESS = "Mouse Press";

		//BUTTONS
		//Button OK
		public static string ANALYTICS_BUTTON_OK = "OK ";
		//Button Cancel
		public static string ANALYTICS_BUTTON_CANCEL = "CANCELA ";
		//Button Enviar
		public static string ANALYTICS_BUTTON_SEND = "ENVIA ";
		//Button Facebook
		public static string ANALYTICS_BUTTON_COMMENT_FACEBOOK = "COMENTA EN FACEBOOK ";
		//Button 
		public static string ANALYTICS_BUTTON_COMMENT_TWITTER = "COMENTA EN TWITTER ";
		//Button 
		public static string ANALYTICS_BUTTON_COMMENT_SMS = "COMENTA CON SMS ";
		//Button
		public static string ANALYTICS_BUTTON_PLAY = "COMENZAR A JUGAR ";
		//Button
		public static string ANALYTICS_BUTTON_GO = "IR A ";

		private static bool NEED_TRACK = true;
		private static string GOOGLE_ANALYTICS_ID = "UA-46614597-1";


		public Utils ()
		{

		}

		public static string sign (String key, List<String> datas){
			Console.WriteLine ("Signing key= "+key+" datas= "+datas);
			String signedData = "";
			foreach (String data in datas)
			{
				signedData = signedData+data;
			}
			byte[] keyBytes = Encoding.ASCII.GetBytes(key);
			byte[] dataBytes = Encoding.ASCII.GetBytes(signedData);

			HMACSHA1 hmac = new HMACSHA1(keyBytes);
			hmac.Initialize ();
			byte[] hmacBytes = hmac.ComputeHash (dataBytes);
			String base64String = System.Convert.ToBase64String (hmacBytes);
			return base64String;
		}

		public static bool saveToStorage(String path, string filename, string text){
			Console.WriteLine ("Saving text= "+text+" to filename= "+filename+" path= "+path);
			String totalPath = Path.Combine(path, filename);
			if (!System.IO.Directory.Exists (path)) {
				Console.WriteLine ("Directory not found. Creating...");
				System.IO.Directory.CreateDirectory (path);
			}
			System.IO.File.WriteAllText(@totalPath, text);
			return true;
		}


		public static string readFromStorage(String path, string filename){
			Console.WriteLine ("Reading from storage...");
			String totalPath = Path.Combine(path, filename);
			return System.IO.File.ReadAllText(@totalPath);
		}

		public static bool existFile (String filename)
		{
			return File.Exists (filename);
		}

		public static Dictionary<String, String> readParametersFromFile(String filename){
			Console.WriteLine ("Reading parameters from filename="+filename);
			if(!File.Exists(filename)){
				Console.WriteLine ("Error. Couldn't read from file " + filename);
				return null;
			}
			Dictionary<String, String> dictionary = File.ReadAllLines(filename)
				.Select(l => l.Split(new[] { '=' }))
				.ToDictionary( s => s[0].Trim(), s => s[1].Trim());
			return dictionary;
		}

		public static Dictionary<String, Dictionary<String, String> > dbConnection(String server,
			String port,
			String user,
			String password,
			String database,
			String query)
		{
			Console.WriteLine ("Query to SQL DB "+query);
			Dictionary<String,Dictionary<String, String>> result = new Dictionary<String, Dictionary<String, String>>();
			NpgsqlConnection connection = new NpgsqlConnection("Server="+server+";Port="+port+";User Id="+user+";Password="+password+";Database="+database+";");
			if(connection==null){
				return result;
			}

			NpgsqlCommand command = new NpgsqlCommand(query, connection);
			
			try
			{
				connection.Open();
				NpgsqlDataReader dr = command.ExecuteReader();
				int columnIndex = 0;
				while(dr.Read())
				{
					Dictionary<String, String> values = new Dictionary<String, String>();
					for (int i = 0; i < dr.FieldCount; i++)
					{
						values.Add(dr.GetName(i), dr[i].ToString());
					}
					String rowName = ""+columnIndex++;
					result.Add(rowName, values);
				}
			}
			catch(Exception e){
				Console.WriteLine ("Error trying to connect to DB "+e);
				return result;
			}

			finally
			{
				connection.Close();
			}
			return result;
		}


		public async static void testSql(){
			Console.WriteLine ("Testing SQL");
			Dictionary<String, Dictionary<String, String> > resultSql = new Dictionary<String, Dictionary<String, String>> ();
			Task a = Task.Factory.StartNew (
				( ) => {
					 resultSql = Utils.dbConnection ("10.5.5.93", "5432", "mp_mobile", "#Dlclfarv27", "mp_mobile", "SELECT * FROM users");
				});

			await a;
			foreach(KeyValuePair<String, Dictionary<String, String>> rows in resultSql){
				String values="";
				foreach(KeyValuePair<String,String> row in rows.Value){
					values = values + " "+row;

				}
				Console.WriteLine (values);
			}
		}

		public static string serializeSession(AppSession a){
			Console.WriteLine ("Serializing "+a.ToString());
			String serialized = JsonConvert.SerializeObject(a);
			Console.WriteLine ("Serialized "+serialized);
			return serialized;
		}

		public static AppSession deserializeSession(String session){
			Console.WriteLine ("Deserializing "+session);
			AppSession deserialized = JsonConvert.DeserializeObject<AppSession>(session);
			Console.WriteLine ("Deserialized "+deserialized.ToString()); 
			return deserialized;
		}


		public static string getAnalyticsID ()
		{
			return GOOGLE_ANALYTICS_ID;
		}

		public static bool needTrack(){
			return NEED_TRACK;
		}
	}
}