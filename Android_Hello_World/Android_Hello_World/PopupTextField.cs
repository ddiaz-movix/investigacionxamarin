using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Android_Hello_World
{
	[Activity (Label = "Enviar SMS")]			
	public class PopupTextField : Activity
	{
		private static string LULI_TO_MESSAGE="Hola Luli, mandame una pregunta!!!!";

		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening PopupTextField...");
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.PopupTextField);
			Button sendMessage = FindViewById<Button> (Resource.Id.sendMessage);
			TextView messageToSend = FindViewById<TextView> (Resource.Id.cellPhoneNumberText);

			sendMessage.Click += (sender, e) => {
				string phoneNumber = messageToSend.Text;
				SmsBroadcast.sendSms(phoneNumber, LULI_TO_MESSAGE);
				Finish ();
			};


		}
	}
}

