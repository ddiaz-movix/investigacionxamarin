using System.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Util;
using Android.Widget;
using Android.Telephony;

using Environment = System.Environment;
using System.Collections.Generic;
using System;

namespace Android_Hello_World
{

		[BroadcastReceiver(Enabled = true, Label = "SMS Receiver")]
		[IntentFilter(new[] { "android.provider.Telephony.SMS_RECEIVED" })] 
	public class SmsBroadcast : Android.Content.BroadcastReceiver 
	{
		public static readonly string INTENT_ACTION = "android.provider.Telephony.SMS_RECEIVED"; 

		public override void OnReceive(Context context, Intent intent)
		{
			if (intent.Action == INTENT_ACTION)
			{
				StringBuilder buffer = new StringBuilder();
				Android.OS.Bundle bundle = intent.Extras;

				if (bundle != null)
				{
					Java.Lang.Object[] pdus = (Java.Lang.Object[])bundle.Get("pdus");

					SmsMessage[] msgs;
					msgs = new SmsMessage[pdus.Length];

					for (int i = 0; i < msgs.Length; i++)
					{
						msgs[i] = SmsMessage.CreateFromPdu((byte[])pdus[i]);

						Log.Info("SmsReceiver", "SMS Received from: " + msgs[i].OriginatingAddress);
						Log.Info("SmsReceiver", "SMS Data: " + msgs[i].MessageBody.ToString());
						Console.WriteLine (msgs[i].MessageBody.ToString());
					}

					Log.Info("SmsReceiver", "SMS Received");
				}
			} 
		}

		public static void sendSms(string to, string message){
			Console.WriteLine ("Sending sms "+message+" to "+to);
			if(to==null || to.Equals("")){
				Console.WriteLine ("Sending sms failed. Not valid destiny.");
				return;
			}
			try{
			SmsManager.Default.SendTextMessage (to, null,
				message, null, null);
			}
			catch(Exception e){
				Console.WriteLine ("Sending sms failed to= "+to+" message= "+message);
				return;
			}
		}
	}
}

