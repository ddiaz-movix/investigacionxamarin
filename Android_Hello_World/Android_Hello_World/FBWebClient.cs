using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Facebook;

namespace Android_Hello_World
{
	class FBWebClient : WebViewClient
	{ 
		FacebookScreen parentActivity;

		public FBWebClient (FacebookScreen parentActivity)
		{
			this.parentActivity = parentActivity;
		}

		public override void OnPageFinished (WebView view, string url)
		{
			base.OnPageFinished (view, url);

			var fb = new FacebookClient ();
			FacebookOAuthResult oauthResult;
			if (!fb.TryParseOAuthCallbackUrl (new Uri (url), out oauthResult))
			{
				return;
			}

			if (oauthResult.IsSuccess)
			{
				// Facebook Granted Token
				var accessToken = oauthResult.AccessToken;
				LoginSucceded (accessToken);
			}
			else
			{
				// user cancelled login
				LoginSucceded (string.Empty);
			}
		}

	private void LoginSucceded(string accessToken)
		{
			var fb = new FacebookClient(accessToken);
			Intent backData = new Intent ();

			fb.GetTaskAsync ("me?fields=id").ContinueWith (t => {
				if(!t.IsFaulted)
				{						
					if (t.Exception != null)
					{
						backData.PutExtra ("AccessToken", accessToken);
						backData.PutExtra ("Exception", t.Exception.Message);

						parentActivity.SetResult (Result.Canceled, backData);
						parentActivity.Finish();
					}

					var result = (IDictionary<string, object>)t.Result;
					var id = (string)result["id"];

					backData.PutExtra ("AccessToken", accessToken);
					backData.PutExtra ("UserId", id);

					parentActivity.SetResult (Result.Ok, backData);
					parentActivity.Finish();
				}
			});
		}
	}
}

