using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Android_Hello_World
{
	[Activity (Label = "HowToScreen")]			
	public class HowToScreen : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening HowToScreen...");
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.howto);

			var buttonHowToOk = FindViewById<Button>(Resource.Id.okButton);

			buttonHowToOk.Click += (sender, e) => {
				this.Finish();
			};
			AndroidUtils.trackView(this, "HowToScreen");


		}
	}
}

