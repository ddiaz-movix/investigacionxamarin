using System;
using System.Net;
using System.IO;
using System.Threading;
using System.Json;
using System.Data;
using System.Collections.Generic;
using System.Diagnostics;
using RestSharp;

namespace Android_Hello_World
{
	public class ServiceRequest
	{
		private ManualResetEvent allDone;
		private string postData;
		private string responseString;
		private string service;
		private bool active;


		public static string START_DATA_SERVICE = "startData";
		public static string PRE_START_DATA_SERVICE = "getPreStartData2";
		public static string BASE_PATH = Environment.GetFolderPath (Environment.SpecialFolder.Desktop);

		private static string MOVIX_SERVICES = "https://secure.movix.com/mp-mobile/android/rest/ejb/";

		private Stopwatch watcher;

		public ServiceRequest(){
			Console.WriteLine ("Creating ServiceRequest...");
			this.allDone = new ManualResetEvent(false);

		}

		public void request (String service, string postData)
		{
			Console.WriteLine ("Called request for service= "+service+" postData"+postData);
			this.active = false;

			try{
				Console.WriteLine("Requesting service " + service + " with post "+postData);
				this.postData = postData;
				this.service = service;
				this.watcher = new Stopwatch();
				this.watcher.Start();
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(MOVIX_SERVICES+this.service);
				request.ContentType = "application/x-www-form-urlencoded";
				request.Method = "POST";
				this.active = true;
				request.Host = "secure.movix.com";
				request.Proxy = null;
				request.Proxy = System.Net.WebRequest.DefaultWebProxy=null;
				System.Net.ServicePointManager.Expect100Continue = false;
				request.ServicePoint.Expect100Continue = false;
				request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);
				allDone.WaitOne ();
				Console.WriteLine ("Ending web request...Took: "+this.watcher.ElapsedMilliseconds/1000.0);
				allDone.Close ();
			}
			catch(WebException e){
				Console.WriteLine ("Error request " + e);
				this.closeWithError();
			}
		}

		private void GetRequestStreamCallback(IAsyncResult asynchronousResult)
		{
			Console.WriteLine ("Asynchronous CallBack asynchronousResult="+asynchronousResult.ToString());
			if(!this.active){
				return;
			}
			try{
				HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
				Stream postStream = request.EndGetRequestStream(asynchronousResult);
				byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(postData);
				postStream.Write(byteArray, 0, postData.Length);
				postStream.Close();
				request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
			}
			catch(WebException e){
				Console.WriteLine ("Error GetRequestStreamCallback"+e);
				this.closeWithError ();
			}
		}

		private void closeWithError ()
		{
			Console.WriteLine ("Closing with Error...");
			this.active = false;
			this.allDone.Set ();
		}

		private void GetResponseCallback(IAsyncResult asynchronousResult)
		{
			Console.WriteLine ("Asynchronouse CallBack asynchronousResult="+asynchronousResult.ToString());
			if(!this.active){
				return;
			}
			try{
				HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;
				HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
				Stream streamResponse = response.GetResponseStream();
				StreamReader streamRead = new StreamReader(streamResponse, System.Text.Encoding.UTF8);
				responseString = streamRead.ReadToEnd();
				streamResponse.Close();
				streamRead.Close();
				response.Close();
				allDone.Set ();
				this.watcher.Stop();
			}
			catch(WebException e){
				Console.WriteLine ("Error GetResponseCallback"+e);
				this.closeWithError ();
			}

		}

		public string getResponseString(){
			return responseString;
		}

		public JsonValue getResponseJsonValue(){
			JsonValue response = null;
			try{
				response = JsonValue.Parse (this.responseString);
			}
			catch(ArgumentException e){
				Console.WriteLine ("Error getResponseJsonValue"+e);
				return response;
			}
			return response;
		}

		public static JsonValue startDataRequest(String pin, string simId, string appName){
			Console.WriteLine ("Called startDataRequest");
			ServiceRequest test = new ServiceRequest ();
			List<String> datas = new List<String> ();
			datas.Add (simId);
			datas.Add (appName);
			String signature = Utils.sign (pin, datas);
			test.request (START_DATA_SERVICE, "signature="+signature+"&simId="+simId+"&appName="+appName);

			return test.getResponseJsonValue();
		}

		public static JsonValue preStartDataRequest(String tag, string appName, string version, string simId){
			Console.WriteLine ("Called preStartDataRequest");
			ServiceRequest test = new ServiceRequest ();
			String dataRequest = "tag="+tag+"&appName="+appName+"&version="+version+"&simId="+simId;
			test.request (PRE_START_DATA_SERVICE, dataRequest);
			return test.getResponseJsonValue();
		}

		public static string preStartDataRestSharpRequest(String service, string tag, string appName, string version, string simId){
			Console.WriteLine ("Called preStartDataRequest using RestSharp");
			Dictionary<String, String> postData = new Dictionary<String, String>();
			postData.Add ("tag", tag);
			postData.Add ("appName", appName);
			postData.Add ("version", version);
			postData.Add ("simId", simId);
			return restRequest (service, postData);
		}

	

		public static bool servicesAvailables(){
			Console.WriteLine ("Checking services available...");
			String postData = "tag=0&appName=test&version=0&simId=0";
			ServiceRequest tester = new ServiceRequest ();
			return tester.connectionOk (postData);
		}

		private bool connectionOk (String postDataTester)
		{
			try{
				Console.WriteLine ("Checking connectivity...");
				this.postData = postDataTester;
				this.service = PRE_START_DATA_SERVICE;
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(MOVIX_SERVICES+this.service);
				request.Timeout = 3000;
				request.ContentType = "application/x-www-form-urlencoded";
				request.Method = "POST";
				this.active = true;
				request.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), request);
				allDone.WaitOne ();
				allDone.Close ();
			}
			catch(WebException e){
				Console.WriteLine ("Error connectionOk " + e);
				this.closeWithError();
				return false;
			}
			Console.WriteLine ("Connectivity OK...");
			return true;
		}

		private static string restRequest(String service, Dictionary<String, String> postData){
			Console.WriteLine ("Called request for service= "+service+" postData"+postData.ToString()+ " using RestSharp.");
			RestClient client = new RestClient (MOVIX_SERVICES);

			RestRequest request = new RestRequest (Method.POST);

			foreach (KeyValuePair<String, String> parameter in postData) {
				request.AddParameter (parameter.Key, parameter.Value);
			}


			IRestResponse response = client.Execute (request);
			return response.Content;

		}

		public static string getMovixServiceUrl(){
			return MOVIX_SERVICES;
		}
	}
}