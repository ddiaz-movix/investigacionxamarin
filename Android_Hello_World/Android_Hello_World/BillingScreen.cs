using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;

namespace Android_Hello_World
{
	[Activity (Label = "BillingScreen")]			
	public class BillingScreen : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening BillingScreen...");
			base.OnCreate (bundle);

			SetContentView(Resource.Layout.bill);
			StartActivity(typeof(QAScreen));
		}
	}
}