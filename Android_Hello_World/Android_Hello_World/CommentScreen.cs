using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Android_Hello_World
{
	[Activity (Label = "CommentScreen")]			
	public class CommentScreen : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening CommentScreen...");
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.comment);

			var buttonSend = FindViewById<Button> (Resource.Id.buttonSend);
			var buttonCancel = FindViewById<Button> (Resource.Id.buttonCancel);

			buttonSend.Click += (sender, e) => {
				this.Finish();
			};

			buttonCancel.Click += (sender, e) => {
				this.Finish();
			};
			AndroidUtils.trackView(this, "CommentScreen");
		}
	}
}

