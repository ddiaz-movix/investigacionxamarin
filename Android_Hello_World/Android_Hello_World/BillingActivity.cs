using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;

namespace Android_Hello_World
{
	[Activity (Label = "BillingActivity")]			
	public class BillingActivity : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			Console.WriteLine ("Opening BillingActivity...");
			base.OnCreate (bundle);
			SetContentView(Resource.Layout.confirm_billing);

			Button buttonOk = FindViewById<Button> (Resource.Id.activateButton);
			buttonOk.Click += (sender, e) => {
				StartActivity(typeof(PopupTextField));
			};

			Button buttonCancel = FindViewById<Button> (Resource.Id.cancelButton);
			buttonCancel.Click += (sender, e) => {
				Finish();
			};

			AndroidUtils.trackView(this, "BillingActivity");
		}

	}
}