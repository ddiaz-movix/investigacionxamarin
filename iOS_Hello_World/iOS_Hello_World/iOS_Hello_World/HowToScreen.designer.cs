// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace iOS_Hello_World
{
	[Register ("HowToScreen")]
	partial class HowToScreen
	{
		[Outlet]
		MonoTouch.UIKit.UIButton buttonHowToOk { get; set; }

		
		void ReleaseDesignerOutlets ()
		{
			if (buttonHowToOk != null) {
				buttonHowToOk.Dispose ();
				buttonHowToOk = null;
			}

			if (buttonHowToOk != null) {
				buttonHowToOk.Dispose ();
				buttonHowToOk = null;
			}
		}
	}
}
