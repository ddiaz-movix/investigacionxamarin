using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Android_Hello_World;

namespace iOS_Hello_World
{
	public partial class PopupTextfield : UIViewController
	{
		public PopupTextfield () : base ("PopupTextfield", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.buttonSendSms.TouchUpInside += (sender, e) => {
				iOSUtils.trackClick("BUTTON SEND SMS");
				String cellPhoneNumber = this.textCellPhoneNumber.Text;
				try{
					SmsBroadcast.sendSms(cellPhoneNumber,"Hola Luli, mandame una pregunta!!!");
				}
				catch(Exception exception){
					Console.WriteLine ("Couldn't send sms to= "+cellPhoneNumber+" exception= "+exception);
					this.NavigationController.PopViewControllerAnimated(true);
					return;
				}
				this.NavigationController.PopViewControllerAnimated(true);
			};
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			iOSUtils.trackScreen("POPUP SCREEN");
		}
	}
}