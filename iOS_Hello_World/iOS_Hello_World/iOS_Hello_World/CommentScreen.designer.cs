// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace iOS_Hello_World
{
	[Register ("CommentScreen")]
	partial class CommentScreen
	{
		[Outlet]
		MonoTouch.UIKit.UIButton buttonFacebook { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton buttonOkComment { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton buttonTwitter { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextView commentText { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (buttonOkComment != null) {
				buttonOkComment.Dispose ();
				buttonOkComment = null;
			}

			if (commentText != null) {
				commentText.Dispose ();
				commentText = null;
			}

			if (buttonTwitter != null) {
				buttonTwitter.Dispose ();
				buttonTwitter = null;
			}

			if (buttonFacebook != null) {
				buttonFacebook.Dispose ();
				buttonFacebook = null;
			}
		}
	}
}
