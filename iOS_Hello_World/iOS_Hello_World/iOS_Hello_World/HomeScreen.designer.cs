// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace iOS_Hello_World
{
	[Register ("HomeScreen")]
	partial class HomeScreen
	{
		[Outlet]
		MonoTouch.UIKit.UIButton buttonComment { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton buttonHowTo { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton buttonLegal { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton buttonPlay { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton buttonPrice { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (buttonComment != null) {
				buttonComment.Dispose ();
				buttonComment = null;
			}

			if (buttonHowTo != null) {
				buttonHowTo.Dispose ();
				buttonHowTo = null;
			}

			if (buttonLegal != null) {
				buttonLegal.Dispose ();
				buttonLegal = null;
			}

			if (buttonPlay != null) {
				buttonPlay.Dispose ();
				buttonPlay = null;
			}

			if (buttonPrice != null) {
				buttonPrice.Dispose ();
				buttonPrice = null;
			}
		}
	}
}
