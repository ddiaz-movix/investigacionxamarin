using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Shared_MP.app.mp;
using Shared_MP.util;
using Shared_MP.communication;
using Shared_MP.app.mp.manager;

namespace iOS_Hello_World
{
	public partial class SplashScreen : UIViewController
	{
		private HomeScreen homeScreen;
		private static LogClass logger = new LogClass();

		public SplashScreen () : base ("SplashScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			//HomeScreen
			this.buttonSplash.TouchUpInside += (sender, e) => {
				if(this.homeScreen == null)
				{ this.homeScreen = new HomeScreen(); }
				iOSUtils.trackClick(AnalyticsUtil.ANALYTICS_BUTTON_GO+"SplashScreen");
				initAndGoHome();
				this.NavigationController.PushViewController(this.homeScreen, true);
			};
		}

		private void initAndGoHome(){
			AppSession.CreateSession ();
			AppClient.GetAppClient ().GetStartData ();
			AppClient.GetAppClient ().GetPreStartData (AppSession.GetSession().GetTag(), 
				AppSession.GetSession().GetAppName(),
				AppSession.GetSession().GetVersionCode(),
				AppSession.GetSession().GetSimSerialNumber());
			MegapromoManager.getCurrentStatus (AppSession.GetSession ().GetUniqueIdentifier());
			Console.WriteLine(AppSession.GetSession ().GetPreStartData ().ToString ());
			Console.WriteLine(AppSession.GetSession ().GetStartData ().ToString ());

		}
	}
}