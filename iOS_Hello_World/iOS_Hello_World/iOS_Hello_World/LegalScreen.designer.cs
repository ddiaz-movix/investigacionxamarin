// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;
using MonoTouch.UIKit;

namespace iOS_Hello_World
{
	[Register ("LegalScreen")]
	partial class LegalScreen
	{
		[Outlet]
		MonoTouch.UIKit.UIButton buttonLegalOk { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel labelLegalText { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (labelLegalText != null) {
				labelLegalText.Dispose ();
				labelLegalText = null;
			}

			if (buttonLegalOk != null) {
				buttonLegalOk.Dispose ();
				buttonLegalOk = null;
			}
		}
	}
}
