using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using GoogleAnalytics.iOS;
using Android_Hello_World;

namespace iOS_Hello_World
{
	public partial class HowToScreen : UIViewController
	{
		public HowToScreen () : base ("HowToScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.buttonHowToOk.TouchUpInside += (sender, e) => {
				iOSUtils.trackClick(Utils.ANALYTICS_BUTTON_OK+"REGLAS");
				this.NavigationController.PopViewControllerAnimated(true);
			};
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			iOSUtils.trackScreen ("HowTo Screen");
		}
	}
}