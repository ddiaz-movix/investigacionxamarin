// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace iOS_Hello_World
{
	[Register ("PlayScreen")]
	partial class PlayScreen
	{
		[Outlet]
		MonoTouch.UIKit.UIButton buttonPlayCancel { get; set; }


		[Outlet]
		MonoTouch.UIKit.UIButton buttonPlayOk { get; set; }

		
		void ReleaseDesignerOutlets ()
		{
			if (buttonPlayOk != null) {
				buttonPlayOk.Dispose ();
				buttonPlayOk = null;
			}

			if (buttonPlayCancel != null) {
				buttonPlayCancel.Dispose ();
				buttonPlayCancel = null;
			}

		}
	}
}
