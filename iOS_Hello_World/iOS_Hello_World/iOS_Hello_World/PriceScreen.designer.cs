// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace iOS_Hello_World
{
	[Register ("PriceScreen")]
	partial class PriceScreen
	{
		[Outlet]
		MonoTouch.UIKit.UIButton buttonPriceOk { get; set; }

		
		void ReleaseDesignerOutlets ()
		{
			if (buttonPriceOk != null) {
				buttonPriceOk.Dispose ();
				buttonPriceOk = null;
			}

			if (buttonPriceOk != null) {
				buttonPriceOk.Dispose ();
				buttonPriceOk = null;
			}
		}
	}
}
