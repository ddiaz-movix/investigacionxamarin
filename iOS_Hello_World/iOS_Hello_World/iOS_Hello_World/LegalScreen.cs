using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using GoogleAnalytics.iOS;
using Shared_MP.app.mp;
using Shared_MP.util;

namespace iOS_Hello_World
{
	public partial class LegalScreen : UIViewController
	{


		public LegalScreen () : base ("LegalScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.buttonLegalOk.TouchUpInside += (sender, e) => {
				iOSUtils.trackClick(AnalyticsUtil.ANALYTICS_BUTTON_OK+"BASES");
				this.NavigationController.PopViewControllerAnimated(true);
			};
			this.labelLegalText.Text = iOSAppSession.GetSession ().GetBases();
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			iOSUtils.trackScreen ("Legal Screen");
		}
	}
}

