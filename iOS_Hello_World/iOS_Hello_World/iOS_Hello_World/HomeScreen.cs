using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;
using GoogleAnalytics.iOS;
using Shared_MP.app.mp;
using Shared_MP.util;
using Shared_MP.common;
using Shared_MP.communication;
using Android_Hello_World;

namespace iOS_Hello_World
{
	public partial class HomeScreen : UIViewController
	{
		LegalScreen legalScreen;
		HowToScreen howToScreen;
		CommentScreen commentScreen;
		PlayScreen playScreen;
		PriceScreen priceScreen;

		public HomeScreen () : base ("HomeScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			//Bases
			this.buttonLegal.TouchUpInside += (sender, e) => {
				if(this.legalScreen == null)
				{ this.legalScreen = new LegalScreen(); }
				iOSUtils.trackClick(AnalyticsUtil.ANALYTICS_BUTTON_GO+"Bases");
				this.NavigationController.PushViewController(this.legalScreen, true);
			};
			//Reglas
			this.buttonHowTo.TouchUpInside += (sender, e) => {
				if(this.howToScreen == null)
				{ this.howToScreen = new HowToScreen(); }
				iOSUtils.trackClick(AnalyticsUtil.ANALYTICS_BUTTON_GO+"Reglas");
				this.NavigationController.PushViewController(this.howToScreen, true);
			};
			//Feedback
			this.buttonComment.TouchUpInside += (sender, e) => {
				if(this.commentScreen == null)
				{ this.commentScreen = new CommentScreen(); }
				iOSUtils.trackClick(AnalyticsUtil.ANALYTICS_BUTTON_GO+"Comentar");
				this.NavigationController.PushViewController(this.commentScreen, true);
			};
			//Jugar
			this.buttonPlay.TouchUpInside += (sender, e) => {
				if(this.playScreen == null)
				{ this.playScreen = new PlayScreen(); }
				iOSUtils.trackClick(AnalyticsUtil.ANALYTICS_BUTTON_GO+"Jugar");
				this.NavigationController.PushViewController(this.playScreen, true);
			};
			//Premios
			this.buttonPrice.TouchUpInside += (sender, e) => {
				if(this.priceScreen == null)
				{ this.priceScreen = new PriceScreen(); }
				iOSUtils.trackClick(AnalyticsUtil.ANALYTICS_BUTTON_GO+"Regalos");
				this.NavigationController.PushViewController(this.priceScreen, true);
			};
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			iOSUtils.trackScreen ("Home Screen");

		}
	}
}