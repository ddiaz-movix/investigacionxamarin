using System;
using Android_Hello_World;
using GoogleAnalytics.iOS;
using System.Collections.Generic;

namespace iOS_Hello_World
{
	public class iOSUtils
	{
		public iOSUtils ()
		{
		}

		public static void trackScreen(string screenName){
			if(Utils.needTrack()){
				GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, screenName);
				GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateAppView ().Build ());
			}
		}

		public static void trackClick( string buttonInformation){
			if(Utils.needTrack()){
				GAIDictionaryBuilder click = GAIDictionaryBuilder.CreateEvent( 
					Utils.ANALYTICS_MOUSE_CATEGORY,
					Utils.ANALYTICS_MOUSE_CLICK,
					buttonInformation,
					null);
				GAI.SharedInstance.DefaultTracker.Send(click.Build ());
			}
				
		}

		public static void trackClick(string buttonEvent, string buttonInformation){
			if(Utils.needTrack()){
				GAIDictionaryBuilder click = GAIDictionaryBuilder.CreateEvent( 
					"ui_action",
					buttonEvent,
					buttonInformation,
					null);
				GAI.SharedInstance.DefaultTracker.Send(click.Build ());
			}

		}
	}
}