// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace iOS_Hello_World
{
	[Register ("PopupTextfield")]
	partial class PopupTextfield
	{
		[Outlet]
		MonoTouch.UIKit.UIButton buttonSendSms { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField textCellPhoneNumber { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (buttonSendSms != null) {
				buttonSendSms.Dispose ();
				buttonSendSms = null;
			}

			if (textCellPhoneNumber != null) {
				textCellPhoneNumber.Dispose ();
				textCellPhoneNumber = null;
			}

			if (buttonSendSms != null) {
				buttonSendSms.Dispose ();
				buttonSendSms = null;
			}
		}
	}
}
