using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Android_Hello_World;
using GoogleAnalytics.iOS;

namespace iOS_Hello_World
{
	public partial class PlayScreen : UIViewController
	{
		PopupTextfield popupTextfield;


		public PlayScreen () : base ("PlayScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.buttonPlayOk.TouchUpInside += (sender, e) => {
				iOSUtils.trackClick(Utils.ANALYTICS_BUTTON_OK+"JUGAR");
				if(this.popupTextfield==null){
					this.popupTextfield = new PopupTextfield();
				}
				this.NavigationController.PushViewController(this.popupTextfield, true);
			};
			this.buttonPlayCancel.TouchUpInside += (sender, e) => {
				iOSUtils.trackClick(Utils.ANALYTICS_BUTTON_PLAY+"JUGAR");
				this.NavigationController.PopViewControllerAnimated(true);
			};
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			iOSUtils.trackScreen("PLAY SCREEN");
		}
	}
}