using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using Facebook;
using System.Collections.Generic;
using GoogleAnalytics.iOS;
using Android_Hello_World;

namespace iOS_Hello_World
{
	public partial class PriceScreen : UIViewController
	{

		public PriceScreen () : base ("PriceScreen", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{

			base.ViewDidLoad ();
			FacebookScren facebookScreen = new FacebookScren (Android_Hello_World.FacebookUtils.getAppId(), Android_Hello_World.FacebookUtils.getExtendedPermissions());
			RootElement rootElement = new RootElement ("Facebook Login") {
				new Section ("Status") {
					new StyledStringElement ("Not Logged In") {
						BackgroundColor = UIColor.Red,
						Alignment = UITextAlignment.Center
					}
				},
				new Section ("Actions") {
					facebookScreen,
					new StringElement ("Graph API Sample", facebookScreen.GraphApiSample),
					new StringElement ("FQL Samplel", facebookScreen.FqlSample),
					new StringElement ("Post \"Hi\" to your wall", facebookScreen.PostHiToWall),
					new StringElement ("Remove \"Hi\" from your wall", facebookScreen.RemoveHiFromWall),
				}
			};


			this.buttonPriceOk.TouchUpInside += (sender, e) => {
				iOSUtils.trackClick(Utils.ANALYTICS_BUTTON_OK+"PREMIOS");
				DialogViewController dvcController = new DialogViewController (rootElement,true);
				this.NavigationController.PushViewController (dvcController, true);
			};
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			iOSUtils.trackScreen ("PRICE SCREEN");
		}
	}
}

