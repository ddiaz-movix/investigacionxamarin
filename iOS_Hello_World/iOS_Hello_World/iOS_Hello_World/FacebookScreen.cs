using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using Facebook;
using System.Collections.Generic;

namespace iOS_Hello_World
{
	public class FacebookScren : Element
	{
		NSUrl nsUrl;
		static NSString hkey = new NSString ("FacebookScren");
		UIWebView web;
		FacebookClient _fb;

		DialogViewController dvcController;
		bool isLoggedIn = false;
		String lastMessageId;
		RootElement friends;
		String uri;

		public FacebookScren (String appId, string extendedPermissions) : base ("Facebook Log In")
		{
			Console.WriteLine ("Loading FacebookScren appId="+appId+" extendedPermissions= "+extendedPermissions);
			_fb = new FacebookClient ();
			uri = GetFacebookLoginUrl (appId, extendedPermissions);
			nsUrl = new NSUrl (uri);
			Console.WriteLine ("Url is uri= "+uri+ "nsUrl= "+nsUrl);
		}

		protected override NSString CellKey {
			get {
				return hkey;
			}
		}

		public string Url {
			get {
				return nsUrl.ToString ();
			}
		}

		public override UITableViewCell GetCell (UITableView tv)
		{
			var cell = tv.DequeueReusableCell (CellKey);
			if (cell == null){
				cell = new UITableViewCell (UITableViewCellStyle.Default, CellKey);
				cell.SelectionStyle = UITableViewCellSelectionStyle.Blue;
			}
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;

			cell.TextLabel.Text = Caption;
			return cell;
		}

		static bool NetworkActivity {
			set {
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = value;
			}

			get {
				return UIApplication.SharedApplication.NetworkActivityIndicatorVisible;
			}
		}

		// We use this class to dispose the web control when it is not
		// in use, as it could be a bit of a pig, and we do not want to
		// wait for the GC to kick-in.
		class WebViewController : UIViewController {
			FacebookScren container;

			public WebViewController (FacebookScren container) : base ()
			{
				this.container = container;
			}

			public bool Autorotate { get; set; }

			public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
			{
				return Autorotate;
			}
		}

		public override void Selected (DialogViewController dvc, UITableView tableView, NSIndexPath path)
		{
			var vc = new WebViewController (this) {
				Autorotate = dvc.Autorotate
			};

			web = new UIWebView (UIScreen.MainScreen.Bounds) {
				BackgroundColor = UIColor.White,
				AutoresizingMask = UIViewAutoresizing.All
			};

			// We delete cache and cookies so it does not remember our login information
			DeleteCacheandCookies ();


			web.LoadStarted += (webview, e) => {
				NetworkActivity = true;
			};

			web.LoadFinished += (webview, e) =>  {
				NetworkActivity = false;
				var wb = webview as UIWebView;
				FacebookOAuthResult oauthResult;
				if (!_fb.TryParseOAuthCallbackUrl (new Uri (wb.Request.Url.ToString()), out oauthResult))
				{
					return;
				}

				if (oauthResult.IsSuccess)
				{
					// Facebook Granted Token
					var accessToken = oauthResult.AccessToken;
					LoginSucceded(accessToken, dvc);
				}
				else
				{
					// user cancelled login
					LoginSucceded(String.Empty, dvc);
				}
			};

			web.LoadError += (webview, args) => {
				if (args.Error != null && !NetworkActivity){
					Console.WriteLine("LoadError on wb.LoadError error= "+args.Error.LocalizedDescription);
					web.LoadHtmlString (
						String.Format ("<html><center><font size=+5 color='red'>{0}:<br>{1}</font></center></html>",
							"An error occurred: ", args.Error.LocalizedDescription), null);
				}
			};
			vc.NavigationItem.Title = Caption;

			vc.View.AutosizesSubviews = true;
			vc.View.AddSubview (web);

			dvc.ActivateController (vc);

			NetworkActivity = true;
			web.LoadRequest (NSUrlRequest.FromUrl (nsUrl));

		}

		private string GetFacebookLoginUrl (String appId, string extendedPermissions)
		{
     		var parameters = new Dictionary<String, object>();
			parameters["client_id"] = appId;
			parameters ["redirect_uri"] = Android_Hello_World.FacebookUtils.getRedirectFacebookUri ();
			parameters["response_type"] = "token";
			parameters["display"] = "touch";

			// add the 'scope' only if we have extendedPermissions.
			if (!String.IsNullOrEmpty (extendedPermissions))
			{
				// A comma-delimited list of permissions
				parameters["scope"] = extendedPermissions;
			}
			Console.WriteLine ("GetFacebookLoginUrl appId= "+appId+" extendedPermissions= "+extendedPermissions);
			return _fb.GetLoginUrl(parameters).AbsoluteUri;
		}

		private void LoginSucceded(String accessToken, DialogViewController dvc)
		{
			if(accessToken==null || accessToken.Equals("")){
				Console.WriteLine ("Error on LoginSucceded accessToken="+accessToken+" dvc="+dvc.ToString());
				return;
			}
			var fb = new FacebookClient(accessToken);

			fb.GetTaskAsync ("me?fields=id").ContinueWith (t => {
				if(!t.IsFaulted)
				{
					var appDelegate = UIApplication.SharedApplication.Delegate as AppDelegate;

					if (t.Exception != null)
					{
						Console.WriteLine("Error on LoginSucceded t="+t.ToString()+" accessToken="+accessToken+" dvc="+dvc.ToString());
						FacebookLoggedIn (false, accessToken, null, t.Exception);
						dvc.NavigationController.PopViewControllerAnimated (true);
						return;
					}

					var result = (IDictionary<String, object>)t.Result;
					var id = (String)result["id"];
					appDelegate.BeginInvokeOnMainThread ( () => {
						FacebookLoggedIn (true, accessToken, id, null);
						dvc.NavigationController.PopViewControllerAnimated (true);
					});
				}
			});
		}

		private void DeleteCacheandCookies ()
		{
			NSUrlCache.SharedCache.RemoveAllCachedResponses ();
			NSHttpCookieStorage storage = NSHttpCookieStorage.SharedStorage;

			foreach (var item in storage.Cookies) {
				storage.DeleteCookie (item);
			}
			NSUserDefaults.StandardUserDefaults.Synchronize ();
		}

		#region Actions Handlers
		public void GraphApiSample ()
		{
			if (isLoggedIn) {

				_fb.GetTaskAsync ("me").ContinueWith (t => {
					if (!t.IsFaulted) {

						if (t.Exception != null) {
							new UIAlertView ("Couldn't Load Profile", "Reason: " + t.Exception.Message, null, "Ok", null).Show ();
							return;
						}
						var result = (IDictionary<String, object>)t.Result;

						String data = "Name: " + (String)result["name"] + "\n" + 
						              "First Name: " + (String)result["first_name"] + "\n" +
						              "Last Name: " + (String)result["last_name"] + "\n" +
						              "Profile Url: " + (String)result["link"];
						var appDelegate = UIApplication.SharedApplication.Delegate as AppDelegate;
						appDelegate.BeginInvokeOnMainThread ( () => {
							new UIAlertView ("Your Info", data, null, "Ok", null).Show ();
						});
					}
				});
			} else {
				new UIAlertView ("Not Logged In", "Please Log In First", null, "Ok", null).Show();
			}
		}

		public void FqlSample ()
		{			
			// query to get all the friends
			var query = String.Format("SELECT uid,name,pic_square FROM user WHERE uid IN (SELECT uid2 FROM friend WHERE uid1={0}) ORDER BY name ASC", "me()");

			if (isLoggedIn) {

				_fb.GetTaskAsync ("fql", new { q = query }).ContinueWith (t => {
					if (!t.IsFaulted) {
						if (t.Exception != null) {
							new UIAlertView ("Couldn't Load Info", "Reason: " + t.Exception.Message, null, "Ok", null).Show ();
							return;
						}
						var result = (IDictionary<String, object>)t.Result;
						var data = (IList<object>)result["data"];

						var count = data.Count;

						if (dvcController.Root[0].Elements.Count == 3) {

							var appDelegate = UIApplication.SharedApplication.Delegate;
							appDelegate.InvokeOnMainThread ( () => {
								dvcController.Root[0].Elements.RemoveAt(2);
								dvcController.ReloadData();
							});
						}

						friends = new RootElement(count + " friends");
						var section = new Section ();
						foreach (IDictionary<String, object> friend in data) {
							section.Add (new StringElement ((String)friend["name"]));
						}

						friends.Add (section);

						var appDel = UIApplication.SharedApplication.Delegate;
						appDel.BeginInvokeOnMainThread ( ()=> {
							dvcController.Root[0].Add (friends);
							new UIAlertView ("Info", "You have " + count + " friend(s).", null, "Ok", null).Show();
						});
					}
				});
			} else {
				new UIAlertView ("Not Logged In", "Please Log In First", null, "Ok", null).Show();
			}
		}

		public void PostHiToWall ()
		{
			if (isLoggedIn) {
				_fb.PostTaskAsync ("me/feed", new { message = "Hi" }).ContinueWith (t => {
					if (!t.IsFaulted) {
						if (t.Exception != null) {
							new UIAlertView ("Couldn't Load Info", "Reason: " + t.Exception.Message, null, "Ok", null).Show ();
							return;
						}

						var result = (IDictionary<String, object>)t.Result;
						lastMessageId = (String)result["id"];

						var appDelegate = UIApplication.SharedApplication.Delegate as AppDelegate;
						appDelegate.BeginInvokeOnMainThread ( ()=> {
							new UIAlertView ("Success", "You have posted \"Hi\" to your wall. Id: " + lastMessageId, null, "Ok", null).Show ();
						});
					}
				});
			} else {
				new UIAlertView ("Not Logged In", "Please Log In First", null, "Ok", null).Show();
			}
		}

		public void RemoveHiFromWall ()
		{
			if (isLoggedIn) {
				if (String.IsNullOrEmpty (lastMessageId)) {
					new UIAlertView ("Error", "Please Post \"Hi\" to your wall first", null, "Ok", null).Show();
					return;
				}

				_fb.DeleteTaskAsync (lastMessageId).ContinueWith (t => {
					if (!t.IsFaulted) {
						if (t.Exception != null) {
							new UIAlertView ("Couldn't Load Info", "Reason: " + t.Exception.Message, null, "Ok", null).Show ();
							return;
						}
						var appDelegate = UIApplication.SharedApplication.Delegate as AppDelegate;
						appDelegate.BeginInvokeOnMainThread ( ()=> {
							new UIAlertView ("Success", "You have deleted \"Hi\" from you wall.", null, "Ok", null).Show ();
						});
						lastMessageId = null;
					}
				});
			} else {
				new UIAlertView ("Not Logged In", "Please Log In First", null, "Ok", null).Show();
			}
		}

		public void FacebookLoggedIn (bool didLogIn, string accessToken, string userId, Exception error)
		{
			if (didLogIn) {
				RootElement root = this.GetImmediateRootElement();
				var statusElement = root [0].Elements [0] as StyledStringElement;
				statusElement.Caption = "Logged In";
				statusElement.BackgroundColor = UIColor.Green;

				_fb = new FacebookClient (accessToken);

				_fb.GetTaskAsync ("me").ContinueWith( t => {
					if (!t.IsFaulted) {

						if (t.Exception != null) {
							new UIAlertView ("Couldn't Load Profile", "Reason: " + t.Exception.Message, null, "Ok", null).Show ();
							return;
						}
						var result = (IDictionary<String, object>)t.Result;

						String profilePictureUrl = String.Format("https://graph.facebook.com/{0}/picture?type={1}&access_token={2}", userId, "square", accessToken);
						UIImage profileImage = UIImage.LoadFromData (NSData.FromUrl (new NSUrl (profilePictureUrl)));

						String profileName = (String)result["name"];

						var profile = new ImageStringElement (profileName, profileImage);

						var appDelegate = UIApplication.SharedApplication.Delegate as AppDelegate;
						appDelegate.BeginInvokeOnMainThread ( ()=> {
							root[0].Add (profile);
						});

						isLoggedIn = true;
					}
				});

			} else {
				new UIAlertView ("Failed to Log In", "Reason: " + error.Message, null, "Ok", null).Show();
			}
		}
		#endregion

	}
}