using System;
using Shared_MP.app.mp;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreTelephony;

namespace iOS_Hello_World
{
	public class iOSAppSession : AppSession
	{
		NSUuid deviceInformarion;
		CTCarrier networkInformation;


		protected iOSAppSession () : base()
		{
			deviceInformarion = UIDevice.CurrentDevice.IdentifierForVendor;
		}

		public static new AppSession createSession ()
		{
			Console.WriteLine ("Creating iOSSession... ");
			if (AppSession.session == null) {
				AppSession.session = new iOSAppSession ();

			}
			return AppSession.session;
		}


		public override string GetUniqueIdentifier ()
		{
			return deviceInformarion.AsString ();
		}

		public override string GetOperatorName ()
		{
			return networkInformation.CarrierName;
		}

	}
}

