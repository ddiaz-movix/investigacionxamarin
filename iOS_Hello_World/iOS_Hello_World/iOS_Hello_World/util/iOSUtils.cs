using System;
using GoogleAnalytics.iOS;
using System.Collections.Generic;
using Shared_MP.util;

namespace iOS_Hello_World
{
	public class iOSUtils
	{
		public iOSUtils ()
		{
		}

		public static void trackScreen(String screenName){
			if(AnalyticsUtil.needTrack()){
				GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, screenName);
				GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateAppView ().Build ());
			}
		}

		public static void trackClick( string buttonInformation){
			if(AnalyticsUtil.needTrack()){
				GAIDictionaryBuilder click = GAIDictionaryBuilder.CreateEvent( 
					AnalyticsUtil.ANALYTICS_MOUSE_CATEGORY,
					AnalyticsUtil.ANALYTICS_MOUSE_CLICK,
					buttonInformation,
					null);
				GAI.SharedInstance.DefaultTracker.Send(click.Build ());
			}
				
		}

		public static void trackClick(String buttonEvent, string buttonInformation){
			if(AnalyticsUtil.needTrack()){
				GAIDictionaryBuilder click = GAIDictionaryBuilder.CreateEvent( 
					"ui_action",
					buttonEvent,
					buttonInformation,
					null);
				GAI.SharedInstance.DefaultTracker.Send(click.Build ());
			}

		}
	}
}