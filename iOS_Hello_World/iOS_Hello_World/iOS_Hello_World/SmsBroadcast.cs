using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace iOS_Hello_World
{
	public class SmsBroadcast
	{
		public SmsBroadcast ()
		{
		}

		public static void sendSms(String from, string message){
			var smsTo = NSUrl.FromString("sms:18015551234");

			if (UIApplication.SharedApplication.CanOpenUrl(smsTo)) {
				var imessageTo = NSUrl.FromString("sms:"+message);
				Console.WriteLine ("Sending sms: "+message+" to "+from);
				UIApplication.SharedApplication.OpenUrl(imessageTo);
			} else {
				Console.WriteLine ("Couldn open URL for sms: ");
			}
		}
	}
}

