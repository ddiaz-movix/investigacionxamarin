using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.Dialog;
using Facebook;
using GoogleAnalytics.iOS;
using Shared_MP.util;

namespace iOS_Hello_World
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		UIWindow window;
		SplashScreen splashScreen;

		public IGAITracker googleAnalyticsTracker;

		IGAITracker Tracker;

		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			window = new UIWindow (UIScreen.MainScreen.Bounds);
			var rootNavigationController = new UINavigationController();
			splashScreen = new SplashScreen ();
			rootNavigationController.PushViewController (splashScreen, false);
			window.RootViewController = rootNavigationController;
			window.MakeKeyAndVisible ();

			GAI.SharedInstance.DispatchInterval = 5;
			GAI.SharedInstance.TrackUncaughtExceptions = true;
			Tracker = GAI.SharedInstance.GetTracker (AnalyticsUtil.getAnalyticsID());

			return true;




		}

	}
}